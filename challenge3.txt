How to - Challenge 1

*** This tutorial is designed to enable you, the reader, to accomplish the goals outlined in Challenge 1:
	

"You've measured the diffusion of Nickel in MgO and know that the diffusivity should
be D = A * exp(-E/(k*T)) where A is a constant, E is the activation energy, k is Boltzmann's constant, and T is temperature in Kelvin.
Given the data you've collected in diffusivity.txt:
a. What is the activation energy?
b. What is A?"

*** Throughout this tutorial, you will be walked through how to import the necessary tools to construct the program, given descriptions of the individual sections of the program, and given exact programming lines.

*** Lines of programming will be formatted as follows: "type:" will be followed - without spaces - by the exact syntax necessary for that line of code. If indentations are needed, they will be noted and "type:" will be immediately followed by one "*" symbol for EACH indentation level. 





Part 0 - Creating The Program:

Starting in your BASH terminal window, navigate to the file directory and folder containing your Challenge 1 materials. You will now need to tell the terminal that you want to create a new Python program titled "Challenge1.py" using the built- in text editor called "Vim":


type:vim Challenge1.py


This will both create the program on the computer's hard drive, and open up Vim to allow you to begin creating the program. You will need to be familiar with how to use Vim. For a tutorial, please visit: www.openvim.com or locate a suitable Vim tutorial.





Part 1 - Importing The Proper Tools:

To get Started we need tell Python to access the proper modules. These modules are like toolsets, that can be turned on and off.
They give Python access to additional lines of code that help it to do specific tasks. For example, "numpy" is a module that lets us process data in arrays and other formats, and "matplotlib.pyplot" lets us plot data into figures. Some modules must be downloaded from the internet, but these two come standard in the current version of Python.

To activate these modules, we use the command "import". This tells Python to grab a specific module. By following this command with the "as" command, we can designate a new name for our modules so we can refer to them easier.


type:import numpy as np
type:matplotlib.pyplot as plt. 


This will import our modules and allow us to use these shorter names to refer to the modules in the future.





Part 2 - Assigning Data Variables:

This part is simple, we just need to add our data sets and assign them variable names.

We will refer to the Diffusivity data as "D", and the Temperature data as "T".

The data will be in 1 x 3 arrays.

type:D = [7.48e-16, 2.78e-14,1.66e-113]
type:T = [1010,1190,1305]





Part 3 - Converting Values:

To satisfy the questions asked in Challenge 1, we need to convert Diffusivity to the natural logarithm of Diffusivity (ln*D), and the Temperature data to inverse Temperature (1/T).

This is where "numpy" comes in handy. It allows us to use special mathematic functions such as the natural logarithm ln.


type: lnD = [np.log(x) for x in D]
type: invsT = [1/y for y in T]


There a couple things to note here.
First, no math is happening on the left side of the equations; lnD and invsT are just names we are assigning. The data changes are done and located on the left sides of the equations. 

To use "numpy" we type "np.log(x)"; this is saying "use 'numpy's' definition of the natural log and apply it to the value, 'x'".
 
Further in this line, we define "x" by typing "for x in D".

Overall, this gives us the natural log of every value in the variable in D. To keep this as an array, we put the data in brackets.

Similar logic is applied for invsT, however, we don't need "numpy" to do basic arithmetic such as division.

Lastly, we want to display what we get from our manipulations of the data:


type: print("Y values: ", lnD)
type: print("X values: ", invsT)


This is telling the terminal to display the values in "lnd" and "invsT", with the labels "Y values:" and "X values:" beforehand, respectively.





Part 4 - Finding Our Slope and Intercept:

To graph our data, we will need to interpolate our data.

For this assignment, we are assuming a linear relationship between inverse Temperature and the natural log of Diffusivity.
Doing this allows us to define the relationship in slope-intercept form. To find these values we will use "numpy" again. 

"Polyfit" is a command that interpolates data.

First we will conduct a least squares polynomial statistical fit on the data, then we will create a first degree polynomial function from the fit. The reason for this two- step procedure is to allow us to later manipulate and use mathematical operations on our fit function, as well as use the function over any defined domain.


type:fit = np.polyfit(invsT,lnd,1) 
type:fit_fn = np.poly1d(fit)


The command "np.polyfit(invsT,LnD,1)" contains several parts; we are using "numpy" and calling upon the "polyfit" function. According to the user manual on SciPy.org, the syntax of "polyfit" follow the syntax: "numpy.polyfit(x,y,degree). "x" and "y" are naturally the same "x" and "y" that you would need for a proper graph of the function. "degree" refers to the degree of the polynomial that is being constructed.

The second line above "packages" our fit function so that we may later use it, a indicated above. The syntax follows the form: "numpy.poly1d(<polynomial coefficients in decreasing order>)". In this case, "fit" provides the needed coefficients.

Now, we will want to display our slope and intercept data. "numpy" lets us do both at the same time. 


type:slope, intercept = np.polyfit(invsT,LnD,1)


Here, "np.polyfit(invsT,LnD,1)" is formatted in such a way as to designate "slope" as "invsT" and "intercept" as "LnD". The value "1" being the degree of the polynomial, as mentioned before.

We will also be printing in the same way we did before:


type:print("slope: ", slope)
type:print("intercept(A): ", intercept)





Part 5 - Calculating Activation Energy:

Part of Challenge 1 is calculating the Activation Energy.
 
Since we have the slope, we can easily find Activation Energy by using the mathematic relation between Diffusivity and Temperature: D=e^((-Ea/kT)) where:

D = Diffusivity
k = Boltzmann's Constant = 8.6173303*10^5
Ea= Activation Energy
T= Temperature

Taking the natural log of the above equation, we get:  ln(D) = -Ea/(k*T) = -Ea/k *1/T
 
It is easy to see that our slope is equal to the activation energy over Boltzmann's Constant.

Therefore, we can calculate our Activation Energy by multiplying our "slope" value by -k. We also want to print this info:

type:Ea = -(8.6174404*10**-5)*slope
type:print("activation energy: ", Ea)

This calculates and displays our activation energy. Note, that Python recognizes "**" as the exponential designator instead of "^".





Part 6 - Graphical Representation:

To view a graphical representation of how the activation energy and the constant "A", you will now need to create a graph!

First, you will need to Define A Function in Python. Functions in Python function as miniature programs within your program. Sometimes they perform just a small task, other times they perform a huge amount of work for you.

A function can start from scratch or it can start with information you give it. As well, a function can end and "return" a value or other information, or it can simply do its job and end. 

In this case, we will need to make a function who's job is to plot a graph of "D" and "T" as defined in the 5th and 6th lines of the code.



All functions -- this one included -- will start by being defined, named, and given any information they might need to start with. This will be done in one line as:


type:def graph(formula, x_range):


This defines a new function called "graph", and supplies it with two pieces of information it can use to better do its job, called parameters: "formula" and "x_range". Both of these parameters are labels for variables; they are just placeholders that will be filled in at a later stage.



Now that "graph" has been created, you can start to create the graph itself; just like in grade school, you need to create and label your axes, give the graph a title, and tell the graph what you are graphing.

PLEASE NOTE: because you have created a miniature program within the overall program, you will need to indent the following indicated lines of code by ONE indentation each. This formatting tells Python that the indented lines of code belong TO and WITHIN the function defined above them. There can be several "levels" however, for this program, you will only have one level of indentation.



First on the list is to tell "graph" what the x- values and y- values are going to be:


type:*x = np.array(x_range):
type:*y = formula(x):


These two lines serve to tell "graph" what its x and y values will be. 

x, in this case, will be an array (remember, numpy helps us make an array for us, so we call it via "np.<something>") of values to be determined by the "x_range" placeholder variable. (Keep in mind- we will get to the why a little later!)

y, in this case, will be a function of x (just like you learned in Algebra class), and the function itself will be determined by the "formula" placeholder. 

Note how the parentheses work: a part of the program will use the information inside the parentheses to do a job. In this case, x is an array of numbers that is determined and given to it by "x_range"; y is a function that uses x to do its job.  



Now, you will actually construct the graph! All of the following commands to define parts of the plot will start with "plt.<part of graph you're defining>". "plt" is a way of saying "Hey! Plot function of Python! I'm talkin' to you!"


type:*plt.plot(x, y)


This tells Python to make a plot of x vs. y; simple as that.



Now, label your graph!


type:*plt.title('Diffusivity vs. Temperature')



Time to label your x axis:


type:*plt.xlabel('1/T')




Now, label your y axis:


type:*plt.ylabel('ln(D)')



Lastly, you now want to show the world the fruits of your labor!


type:*plt.show()



This reaches the end of the "graph" function and displays your graph - automatically - in a new window.





Part 7 - Supplying "graph" With Information:

In order to give "x-range" a, well, range of values, you will make a new variable called "domain". "domain" will use numpy to call upon a function called "arange". "Arange", to quote the User Manual on SciPy.org, returns an "evenly spaced [set of] values within a given interval"... If this sounds suspiciously like when you drew out tick marks on the x-axis in your Algebra class, this is correct! We are - essentially - telling "domain" that it will represent a range of numbers, all evenly spaced. The range, start and stop points, and spacing between numbers are all going to be given to arange when you call upon it to work. Just like for "graph", you will give Arange some information in parentheses, but this time it follows a specific format:

np.arange([start],[stop],[step])

For this graph, we will be graphing from 0 to 1 with increments of 0.001:

type:domain = np.arange(0,1,0.001)



Now! Remember how "formula" and "x-range" were just placeholder variables? Now it is time to make them do some work! You will now tell your Challenge1.py program to actually graph something, and you will give "formula" and "x-range" their marching orders.

You will call upon the "graph" function you made earlier and give it the information it needs to give you a proper graph. To do so, you will need to both call upon "graph" by its name, and supply it with information that will fill the "formula" and "x-range" placeholders.

The "x-range" placeholder will be filled by the "domain" variable that was just created.

The "formula" placeholder will be filled by "fit_fn", which is the packaged mathematical function used above in Part 4 to give the slope and y intercept of the log of Diffusivity vs inverse Temperature used to answer the question given in Challenge 1:

type:graph(fit_fn,domain)

This is the last step; note how "fit_fn" and "domain are located in the parentheses on top of where "formula" and "x_range" are, respectively? This is how you give "formula" and "x_range" their values to do their jobs with.





Part 8 - Implementation:

Save your work, exit the editor, and go back to your terminal. You will now need to activate the program. Make sure you are within the same file directory and folder as the program you just made. To activate a Python Program from the terminal, you will need to tell the computer that you are in fact asking it to use Python to do something. The syntax will follow the format "python <program file name>".


type:python Challenge1.py


You will then be greeted by the fruits of your labor: Y values, X values, and the Slope, Intercept A and Activation Energy values all printed out on your screen, as well as a graph of the polynomial fit function that gives us our desired and displayed values.


Nice work; you're all done!



