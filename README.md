# README #

Project 3 - Version 1.0.0 




### What is this repository for? ###


* Quick summary

For this project we have several challenges:

   -Challenge 1: We are dealing with the diffusion of nickel in MgO. We solve for what the activation energy (E) is and what A is in the equation D = A * exp(-E/(k * T)). We plot this equation as ln(D) vs 1/T in y = mx+b form.
   
   -Challenge 2: We were given a document containing the x-rays emitted from different elements and the elements atomic numbers. We first used this data and the speed of light to solve for the frequencys and then plot the square roots of the frequencys vs the atomic numbers. Then we used a linear fit of the data just found to find the values of the constants B and C in the equation frequency^(0.5) = B(atomic number - C). Lastly we extracted Planck's constant from our equation to compare to the actual known value of Planck's constant.
   
   -Challenge 3: We created a file on our project 3 directory that contains instructions we wrote. These instructions are so anyone with or without experience of programming can run our challenge one program using codelab.boisestate.edu.
   
   -Challenge 4: A bonus challenge. We were given two images. We needed to find the average and standard deviation of grain sizes in the grain.png image, and we needed to count the number of DNA tiles in dna.tif, with instructions included to exectue the code on codelab.boisestate.edu. 




### How do I get set up? ###

* Summary of set up

The programs used in this project will all be called from the main project directory, which contains all necessary programs and resources.
 
 
 
* Configuration

Challenge 1, Challenge 2, and Challenge 4 programs are run in Python; Challenge 3 is a text file and can be viewed in Vim or any text view/viewer/editor.



* Dependencies

numpy
matplotlib.pyplot



* Database configuration

All information on this project can be found in the project's home directory: p3-team4.



* How to run tests

To run test, type <python<program name>.py> into bash and press enter. 
Challenge 1 program name: challenge1.py
Challenge 2 program name: challenge2.py
Challenge 4 program name: image_analysis.py



* Deployment instructions

To access these programs, navigate in shell to the project home directory. This is where the programs can be run in python.




### Contribution guidelines ###

* Writing tests

This project uses two sets of data found in the diffusivity file and xray file in our projects directory. The the codes written for this project are in the project three directory and can be run on python there. Anything outside the p3-team4 directory is not neccessary for the completion of this project.



* Code review

Files can be pushed or pulled anytime from bitbucket and it is a requirement for members of this project. However, please refrain from breaking or changing any code without reason.



* Other guidelines

Refer to admin for more information on this project or directory.




### Who do I talk to? ###

* Repo owner or admin

Eric Jankowski



* Other community or team contact

Team members and contact information can be found in contributors.txt

