import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from skimage import color

#The following convert the grain.png into a gray scale image then using a threshold makes a blacka and white binary image.
raw = mpimg.imread("grain.png")
chopped = raw[:-200]
gray = color.rgb2gray(chopped)
threshold=0.5
gray[gray<threshold]=0
gray[gray>threshold]=1
plt.imsave("bw.png",gray,cmap=plt.cm.gray)

#grain_count looks at all pixels across a row and counts when a grain boundary is found. In order to find number of grains the number of grain boundaries is divided by two.
def grain_count(row):
    grain =[]
    num = 0
    for i,pix in enumerate(row):
        if pix != row[i-1]:
            num+=1
    return num/2

#grain_size uses the size of the image to estimate grain size. Since grain_count looks at a specific row across the whole image by putting in the number counted and how many total rows were looked at an estimate of grain size in microns is generated.
def grain_size(count,rows):
    line_length=1438.9
    grain=rows*line_length/count
    return grain

#Batch is just a way to add a bunch of lines together. 
batch = grain_count(gray[200])+grain_count(gray[300])+grain_count(gray[400])+ grain_count(gray[500])+ grain_count(gray[600])

print("Number of grains",batch)
print("Grain Size Estimate:", grain_size(batch,5),"um")

#x is 1438.9 um
#y is 1069.4 um
#1.44 pixels per um
