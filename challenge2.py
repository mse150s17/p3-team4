# Challenge 2:

#part a)

#wavelength(s), w, (units: nanometers):
#Mg .987
#Al .834
#S  .573
#Ca .335
#Cr .229
#Fe .194
#Cu .154
#Rb .093
#W  .021

#speed of light, v, v = 2.998E+17 (nanometers/second)

import numpy as np
import matplotlib.pyplot as plt

Wavelength = [.987,.834,.573,.335,.229,.194,.154,.093,.021]
AtomNumber = [12,13,16,20,24,26,29,37,74] 
Speedoflight = (2.998*10**17)

print(" ")
F = [Speedoflight/x for x in Wavelength]
print("Frequency: ",  F)
print(" ")
# defining S as the square root of frequency to plot a more simple variable name

S = [y**0.5 for y in F]
print("Square Root of Frequency: ",  S)
print(" ")

plt.plot(AtomNumber, S)  
plt.title('Square Root of Frequency vs. Atomic Number')
plt.xlabel('Atomic Number')
plt.ylabel('(s^-0.5)')
plt.show()

#part b)

#slope and y intercept of  Root(Frequency) vs. AtomNumber

#B in challenge 2 (part B) is equal to slope
fit = np.polyfit(AtomNumber,S,1)
fit_fn = np.poly1d(fit)

slope, intercept =  np.polyfit(AtomNumber,S,1)
print("B: ",slope) ##Outputted Value of B
print("intercept: ",intercept)

##Outputted value of C
C = intercept/(-slope)

print ("C: " ,C)
print(" ")

#plancks_estimate is a calculation used to check the accuracy of our code
#plancks_estimate uses the first produced Frequency value and the correlating atomic number

plancks_estimate = ((13.6*3*(12-1)**2)/4)/(3.0374873353596755*10**17)
print("Planck's Constant: " ,plancks_estimate)
error = (float(plancks_estimate - 4.135667*10**-15)/float(4.135667*10**-15))*100 #error
print("Planck's Constant Percent Error =", abs(error),"%") #end of error
