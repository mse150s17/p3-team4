import numpy as np
import matplotlib.pyplot as plt

##Starting values for diffusivity and temperature
D = [7.48e-16,2.78e-14,1.66e-13]
T = [1010,1190,1305]

##Natural log of D and 1/T (as needed to solve challenge one)
lnD = [np.log(x) for x in D]
invsT = [1/y for y in T]

##printed what was found, just in case
print("Natural Log of Diffusivity: ",lnD)
print("Inverse of Temperature: ",invsT)

##slope and y intercept of  lnD vs 1/T
fit = np.polyfit(invsT,lnD,1)
fit_fn = np.poly1d(fit)

slope, intercept =  np.polyfit(invsT,lnD,1)
print("Slope: ",slope)
print("Intercept: ",intercept)

##activation energy (related to slope by -boltzmann's constant)
Ea = -(8.6173303*10**-5)*slope
print("Activation energy: " , Ea,"eV")

##The intercept is equal to Ln(A) so to find A e to the intercept is used.
A=np.exp(intercept)
print("Constant A:", A)

##graphical representation
def graph(formula, x_range):  
    x = np.array(x_range)  
    y = formula(x)  
    plt.plot(x, y)  
    plt.title('Diffusivity vs. Temperature')
    plt.xlabel('1/T(K)')
    plt.ylabel('ln(D)(cm^2/s)')
    plt.show()

domain = np.arange(0,1,0.001)

graph(fit_fn,domain)
